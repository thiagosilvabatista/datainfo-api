package com.tsbatistatecnologia.datainfoapi.repositories;

import com.tsbatistatecnologia.datainfoapi.entities.ExternalUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface ExternalUserRepository extends JpaRepository<ExternalUser, Long> , JpaSpecificationExecutor<ExternalUser> {

    @Transactional(readOnly = true)
    @Query(value = "select e from ExternalUser e where e.strCpf = :strCpf")
    Optional<ExternalUser> findByCpf(@Param("strCpf") String strCpf);

}
