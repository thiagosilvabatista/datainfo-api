package com.tsbatistatecnologia.datainfoapi.repositories;

import com.tsbatistatecnologia.datainfoapi.entities.ExternalUserFunction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExternalUserFunctionRepository extends JpaRepository<ExternalUserFunction, Long> {}
