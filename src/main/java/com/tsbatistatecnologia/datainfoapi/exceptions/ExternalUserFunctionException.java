package com.tsbatistatecnologia.datainfoapi.exceptions;

import com.tsbatistatecnologia.datainfoapi.dto.ExternalUserDto;
import com.tsbatistatecnologia.datainfoapi.entities.ExternalUserFunction;

public class ExternalUserFunctionException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private ExternalUserFunction externalUserFunction;
    private ExternalUserDto externalUserDto;



    public ExternalUserFunctionException(final String cause, ExternalUserDto externalUserDto){
        super(cause);
        this.externalUserDto = externalUserDto;
    }

    public ExternalUserFunctionException(final String cause){
        super(cause);
    }

}
