package com.tsbatistatecnologia.datainfoapi.controllers;

import com.tsbatistatecnologia.datainfoapi.dto.ExternalUserDto;
import com.tsbatistatecnologia.datainfoapi.entities.ExternalUser;
import com.tsbatistatecnologia.datainfoapi.services.ExternalUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/external-user")
@CrossOrigin(origins = "*")
public class ExternalUserController {

    @Autowired
    private ExternalUserService externalUserService;

    @GetMapping
    public ResponseEntity<List<ExternalUser>> findAllExternalUser(){
        return new ResponseEntity<List<ExternalUser>>(externalUserService.findAllExternalUser(), HttpStatus.OK);
    }

    @GetMapping(value = "/filter")
    public ResponseEntity<List<ExternalUser>> filterExternalUser(@RequestParam(value = "userName") Optional<String> userName,
                                                           @RequestParam(value = "situation") Optional<String> situation,
                                                           @RequestParam(value = "perfil") Optional<Integer> perfil){
        return new ResponseEntity<List<ExternalUser>>(externalUserService.filterExternalUser(userName.orElse(null), situation.orElse(null), perfil.orElse(null)), HttpStatus.OK);
    }

    @GetMapping(value = "/{numId}")
    public ResponseEntity<ExternalUser> findByCpfExternalUser(@PathVariable final Long numId){
        return new ResponseEntity<ExternalUser>(externalUserService.findById(numId).orElse(null), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ExternalUser> saveExternalUser(@RequestBody @Valid ExternalUserDto externalUserDto) {
        return new ResponseEntity<ExternalUser>(externalUserService.saveExternalUser(externalUserDto), HttpStatus.CREATED);

    }

    @PostMapping(value = "/{numId}")
    public ResponseEntity<ExternalUser> updateExternalUser(@PathVariable final Long numId, @RequestBody @Valid ExternalUserDto externalUserDto) {
        return new ResponseEntity<ExternalUser>(externalUserService.updateExternalUser(externalUserDto, numId), HttpStatus.CREATED);

    }

    @PostMapping(value = "/{numId}/{situation}")
    public ResponseEntity<ExternalUser> updateExternalUser(@PathVariable final Long numId, @PathVariable final String situation ) {
        return new ResponseEntity<ExternalUser>(externalUserService.updateSituation(numId, situation), HttpStatus.CREATED);

    }

    @DeleteMapping(value = "/{numId}")
    public ResponseEntity<String> deleteExternalUser(@PathVariable final Long numId) {
        externalUserService.deleteExternalUser(numId);
        return new ResponseEntity<String>("Usuário deletado com sucesso.", HttpStatus.OK);

    }
}
