package com.tsbatistatecnologia.datainfoapi.controllers;

import com.tsbatistatecnologia.datainfoapi.entities.ExternalUserFunction;
import com.tsbatistatecnologia.datainfoapi.services.ExternalUserFunctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/external-user-function")
public class ExternalUserFunctionController {

    @Autowired
    private ExternalUserFunctionService externalUserFunctionService;

    @GetMapping
    public ResponseEntity<List<ExternalUserFunction>> findByDistrictbyNumId(){
        return new ResponseEntity<List<ExternalUserFunction>>(externalUserFunctionService.getAllExternalUserFunction(), HttpStatus.OK);
    }

}
