package com.tsbatistatecnologia.datainfoapi.services;

import com.tsbatistatecnologia.datainfoapi.entities.ExternalUserFunction;
import com.tsbatistatecnologia.datainfoapi.repositories.ExternalUserFunctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExternalUserFunctionService {

    @Autowired
    private ExternalUserFunctionRepository externalUserFunctionRepository;

    public List<ExternalUserFunction> getAllExternalUserFunction(){
        return externalUserFunctionRepository.findAll();
    }

}
