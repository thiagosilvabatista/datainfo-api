package com.tsbatistatecnologia.datainfoapi.services;


import com.google.common.base.Strings;
import com.tsbatistatecnologia.datainfoapi.dto.ExternalUserDto;
import com.tsbatistatecnologia.datainfoapi.entities.ExternalUser;
import com.tsbatistatecnologia.datainfoapi.entities.ExternalUserFunction;
import com.tsbatistatecnologia.datainfoapi.exceptions.ExternalUserFunctionException;
import com.tsbatistatecnologia.datainfoapi.repositories.ExternalUserFunctionRepository;
import com.tsbatistatecnologia.datainfoapi.repositories.ExternalUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ExternalUserService {

    @Autowired
    private ExternalUserRepository externalUserRepository;

    @Autowired
    private ExternalUserFunctionRepository externalUserFunctionRepository;

    public List<ExternalUser> findAllExternalUser(){
        return externalUserRepository.findAll();
    }

    public Optional<ExternalUser> findByCpf(final String strCpf){
        return externalUserRepository.findByCpf(strCpf);
    }

    public ExternalUser saveExternalUser(final ExternalUserDto externalUserDto){
        try {
            final Optional<ExternalUserFunction> externalUserFunction =  externalUserFunctionRepository.findById(externalUserDto.getNumExternalUserFunction());

            if(!externalUserFunction.isPresent()){
                throw new ExternalUserFunctionException("Função atribuida ao usuário não encontrada.", externalUserDto);
            }

            if(externalUserRepository.findByCpf(externalUserDto.getStrCpf()).isPresent()){
                throw new ExternalUserFunctionException("CPF Já cadastrado.", externalUserDto);
            }


            final ExternalUser externalUser =  new ExternalUser(externalUserDto.getStrCpf(), externalUserDto.getStrName(),
                    externalUserDto.getStrEmail(), externalUserDto.getStrSituation(),
                    externalUserDto.getNumPerfilAccess(), externalUserDto.getStrFone(),
                    externalUserFunction.get());
            return externalUserRepository.save(externalUser);
        }catch (ExternalUserFunctionException e){
            throw new ExternalUserFunctionException(e.getMessage(), externalUserDto);
        }

    }

    public ExternalUser updateExternalUser(final ExternalUserDto externalUserDto, final Long numId){
        try {
            final Optional<ExternalUserFunction> externalUserFunction =  externalUserFunctionRepository.findById(externalUserDto.getNumExternalUserFunction());

            if(!externalUserFunction.isPresent()){
                throw new ExternalUserFunctionException("Função atribuida ao usuário não encontrada.", externalUserDto);
            }

            final ExternalUser externalUser =  new ExternalUser(externalUserDto.getStrCpf(), externalUserDto.getStrName(),
                    externalUserDto.getStrEmail(), externalUserDto.getStrSituation(),
                    externalUserDto.getNumPerfilAccess(), externalUserDto.getStrFone(),
                    externalUserFunction.get());
            externalUser.setNumId(numId);
            return externalUserRepository.save(externalUser);
        }catch (ExternalUserFunctionException e){
            throw new ExternalUserFunctionException(e.getMessage(), externalUserDto);
        }
    }

    public ExternalUser updateSituation(final Long numId, final String situation){
        try {
            final Optional<ExternalUser> externalUser = externalUserRepository.findById(numId);
            externalUser.get().setStrSituation(situation);
            return externalUserRepository.save(externalUser.get());
        }catch (ExternalUserFunctionException e){
            throw new ExternalUserFunctionException(e.getMessage());
        }
    }

    public void deleteExternalUser(final Long numId){
        externalUserRepository.deleteById(numId);
    }

    public List<ExternalUser> filterExternalUser(final String userName, final String situation, final Integer perfil){
        List<ExternalUser> list = externalUserRepository.findAll((root, query, cb) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (!Strings.isNullOrEmpty(userName)) {
                predicates.add(cb.equal(root.get("strName"), userName));
            }

            if (situation != null && situation != "-1") {
                predicates.add(cb.equal(root.get("strSituation"), situation));
            }

            if (perfil != null && perfil >= 0) {
                predicates.add(cb.equal(root.get("numPerfilAccess"), perfil));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        }).stream().collect(Collectors.toList());

        return list;
    }

    public Optional<ExternalUser> findById(final Long numId){
        return externalUserRepository.findById(numId);
    }

}
