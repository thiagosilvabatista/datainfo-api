package com.tsbatistatecnologia.datainfoapi.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "funcao_usuario_externo")
public class ExternalUserFunction {

    @Id
    @Column(name = "co_funcao")
    private Long numFunction;

    @Column(name = "no_funcao")
    private String strFunction;
}
