package com.tsbatistatecnologia.datainfoapi.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Table(name = "usuario_externo")
public class ExternalUser {

    @Id
    @Column(name = "num_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long numId;

    @CPF(message = "Operação não realizada. CPF digitado é inválido.")
    @Size(min = 11, max = 11, message = "CPF invalido")
    @Column(name = "nu_cpf")
    private String strCpf;

    @Column(name = "no_usuario")
    @NotNull(message = "Nome do usuário é obrigatório.")
    @NotEmpty(message = "Nome do usuário é obrigatório.")
    private String strName;

    @Column(name = "de_email")
    @NotNull(message = "E-mail do usário é obrigatório.")
    @NotEmpty(message = "E-mail do usário é obrigatório.")
    private String strEmail;

    @Column(name = "ic_situacao")
    @NotNull(message = "Situação do usuario é obrigatório.")
    @NotEmpty(message = "Situação do usuario é obrigatório.")
    private String strSituation;

    @Column(name = "ic_perfil_acesso")
    private Integer numPerfilAccess;

    @Column(name = "nu_telefone")
    @Size(min = 11, max = 11, message = "Número de telfone invalido.")
    @NotNull(message = "Telefone de usuário é obrigatório.")
    @NotEmpty(message = "Telefone de usuário é obrigatório.")
    private String strFone;

    @JoinColumn(name = "co_funcao")
    @ManyToOne(fetch = FetchType.EAGER)
    private ExternalUserFunction externalUserFunction;

    public ExternalUser(final String strCpf, final String strName, final String strEmail,
                        final String strSituation, final Integer numPerfilAccess, final String strFone,
                        final ExternalUserFunction externalUserFunction){
        this.strCpf = strCpf;
        this.strName = strName;
        this.strEmail = strEmail;
        this.strSituation = strSituation;
        this.numPerfilAccess = numPerfilAccess;
        this.strFone = strFone;
        this.externalUserFunction = externalUserFunction;
    };

    public ExternalUser(){};
}
