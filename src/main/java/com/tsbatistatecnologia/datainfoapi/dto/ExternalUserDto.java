package com.tsbatistatecnologia.datainfoapi.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;


@Getter
@Setter
public class ExternalUserDto {

    @CPF(message = "Operação não realizada. CPF digitado é inválido.")
    private String strCpf;
    private String strName;
    private String strEmail;
    private String strSituation;
    private Integer numPerfilAccess;
    private String strFone;
    private Long numExternalUserFunction;

}
